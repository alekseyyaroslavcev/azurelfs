#include <iostream>
#include <ipps.h>

int main(int argc, char* argv[])
{
    Ipp32f* a = ippsMalloc_32f(100);
    Ipp32f* b = ippsMalloc_32f(100);
    ippsSet_32f(1, a, 100);
    ippsSet_32f(2, b, 100);
    ippsAdd_32f_I(a, b, 100);

}
